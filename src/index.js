var exec = require('child_process').exec;
var through = require('through2');


function gulpDeleteFiles() {
  return through.obj(function (file) {
      var path = file.path;
      return exec('find '+path+' -type f -name "*.*" -exec rm -f {} +');
  });
}

module.exports = gulpDeleteFiles;
